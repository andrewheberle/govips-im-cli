package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/davidbyttow/govips/pkg/vips"
)

const versionString = "0.9.3"

func main() {
	var resize, outFile, outExt, outFormat string
	var quality int
	var showVersion bool

	var width = 0
	var height = 0

	// Named arguments
	flag.BoolVar(&showVersion, "version", false, "Display version info")
	flag.StringVar(&resize, "resize", "", "Resize an image")
	flag.IntVar(&quality, "quality", 80, "Compression quality")
	flag.Parse()

	// Show version
	if showVersion {
		fmt.Fprintf(os.Stdout, "%s\n", versionString)
		os.Exit(0)
	}

	// Positional arguments
	tail := flag.Args()

	if len(tail) == 0 {
		fmt.Fprintf(os.Stderr, "No input file provided\n")
		os.Exit(1)
	}

	// Grab input file
	inFile := strings.TrimSuffix(tail[0], "[0]")
	inExt := path.Ext(inFile)
	inFormat := strings.TrimPrefix(inExt, ".")

	// See if there was a second positional argument given for outfile
	if len(tail) == 2 {
		// Set output file and format
		outFile = tail[1]
		outExt = path.Ext(outFile)
		outFormat = strings.TrimPrefix(outExt, ".")
	} else {
		// Set output file and format to match input file
		outFile = inFile
		outExt = inExt
		outFormat = inFormat
	}
	fmt.Fprintf(os.Stderr, "govips-im-cli : inFile = %s\n", inFile)
	fmt.Fprintf(os.Stderr, "govips-im-cli : outFile = %s\n", outFile)

	// Set default format
	var imgFormat = vips.ImageTypeJPEG

	// Set format
	switch outFormat {
	case "jpg", "jpeg":
		imgFormat = vips.ImageTypeJPEG
	case "png":
		imgFormat = vips.ImageTypePNG
	case "webp":
		imgFormat = vips.ImageTypeWEBP
	case "svg":
		imgFormat = vips.ImageTypeSVG
	case "gif":
		imgFormat = vips.ImageTypeGIF
	default:
		fmt.Fprintf(os.Stderr, "Unsupported output format: %s\n", outFormat)
		os.Exit(1)
	}

	// Create new transform
	imgTransform := vips.NewTransform().
		LoadFile(inFile).
		Quality(quality).
		Format(imgFormat).
		StripMetadata()

		// Ensure transform was created
	if imgTransform == nil {
		fmt.Fprintf(os.Stderr, "Error creating transform: %s\n", inFile)
		os.Exit(1)
	}

	// Handle ressize request
	if resize != "" {
		if strings.HasSuffix(resize, "x") {
			// Resize = WIDTHx
			width, _ = strconv.Atoi(strings.TrimSuffix(resize, "x"))
			imgTransform = imgTransform.ResizeWidth(width)
		} else if strings.HasPrefix(resize, "x") {
			// Resize = xHEIGHT
			height, _ = strconv.Atoi(strings.TrimPrefix(resize, "x"))
			imgTransform = imgTransform.ResizeHeight(height)
		} else if strings.Contains(resize, "x") {
			// Resize = WIDTHxHEIGHT
			rsplit := strings.Split(resize, "x")
			width, _ = strconv.Atoi(rsplit[0])
			height, _ = strconv.Atoi(rsplit[1])
			imgTransform = imgTransform.Resize(width, height)
		} else {
			// Resize = WIDTH
			width, _ = strconv.Atoi(resize)
			imgTransform = imgTransform.ResizeWidth(width)
		}
		fmt.Fprintf(os.Stderr, "govips-im-cli : resize - width = %d\n", width)
	}

	// Set output
	imgTransform = imgTransform.OutputFile(outFile)

	// Start up vips and defer vips shutdown
	vips.Startup(nil)
	defer vips.Shutdown()

	// Apply transform
	_, _, err := imgTransform.Apply()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
